![Screenshot preview of the theme "Midnight Rose" by glenthemes](https://static.tumblr.com/gtjt4bo/g3Rsdjvl0/midnight_rose_pv.png)

**Theme no.:** 02  
**Theme name:** Midnight Rose  
**Theme type:** Free / Tumblr use  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-08-10](https://64.media.tumblr.com/ccb5452d623e505651fb0e780d207f90/tumblr_nsuitxwtIz1ubolzro1_r1_1280.png)  
**Rework date:** [2016-12-14](https://64.media.tumblr.com/f6280d825c80d053033eeafabb4355ed/tumblr_nsuitxwtIz1ubolzro2_r1_1280.jpg)  
**Last updated:** 2024-05-15

**Post:** [glenthemes.tumblr.com/post/154473486819](https://glenthemes.tumblr.com/post/154473486819)  
**Preview:** [glenthpvs.tumblr.com/midnight-rose](https://glenthpvs.tumblr.com/midnight-rose)  
**Download:** [glen-themes.gitlab.io/thms/02/midnight-rose](https://glen-themes.gitlab.io/thms/02/midnight-rose)  
